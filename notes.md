
# A language to realize value

## Background

Our team is interested to develop its capabilities to be able to provide value to customers.
There is a view [here](https://trello.com/b/HgzKG8Hl/capabilities) of the capabilities that are likely to be saleable to customers.
There are following types of things there:
- Short Interval Control
- Value Stream Optimization
- Real Time Bottlenck Analysis
- Mine Value Flow

## Motivation

The business needs a relatively universal language to express the problems which is repetitively solves such that it can develop intellectual property that makes it more efficient to solve such problems in the future and as such provides a competitive advantage.

## Synopsis

We need a language intended to express solutions to the problems that we deal with regularly and is intended to support rapid delivery of quality solutions.
We can be in control of the evolution of this language and can optimize to deliver value.
But we want it to be at least comparable to languages the business already uses.

The intention here is to reflect what it needs to provide to supercede ...spreadsheets, knime.

## The spreadsheet problem

The language (excel spreadsheets) the business currently has been using is problematic for a number of reasons:
- Poor capability to develope *Structured Logic* such that solutions for one business problem can be transferred to the next business problem
- spreadsheets are difficult to concurrently collborate on, 
- that data may be hidden or lost
  - think of all the information on the NAS drive which is lost because it is embedded in a spreadsheet and the spreadsheet is copied and then you end up with confusion about what is source data and derived data.
- may be wrong ... we have no idea how many bugs there are on the NAS drive
- development is usually informal without any planning
- developed without standard SDLC practices
- have no quality assurance
- there are no audit trails
- does not work for *Real Time Data*
- does not work for *Infinite Data Stream*
- *Automatation* challenges : time consuming and error prone to automate multiple steps as excels is focused on end user control over script control
- repetition in spreadsheets makes code review untennable
- *Copy and Paste* errors : encourage copy and paste programming
- Ad Hoc Fudging: encourage ad hoc fudging
- Spreadsheets are poor at composition and we end up make lots of copies of the same spreadsheet which then leads to confusion about the relavence of information in any one spreadsheet.

All of which makes for difficulty to re-use functionality from one consulting engagement to the next.

Take away - Dont use spreadsheets for important work [*Use spreadsheets when the cost of errors is minor*](https://lemire.me/blog/2014/05/23/you-shouldnt-use-a-spreadsheet-for-important-work-i-mean-it/)

## What do we want to retain from a spreadsheet

- *Accessibility* : it can be expected that users have the tools to view
- *Joining* : Joining data with processing logic
- *Ease of data entry*
- *Ease of data presentation* : charting
- *Backwards compatibility* - Microsoft does a very good job of this
- *Self contained distributibility*
- *Self Reliance* : Users dont have reliance on IT help
- *Flexible* : Spreadsheets are pretty flexible...but not as flexible as general purpose programming languages.
- *Collaboaration*: Supports collaboaration well
- *Secure* : users are in control of IP and Data 
- *Interoperability* : works with lots of other technologies

## Language Ideas

### Philosophy

Native data format is Distributable Persistent Log - Distributed     Persistent Log
Log is processed as a stream of data events - Stream Processing
Stream processing is a visualizable directed graph of processing nodes - Directed Graph
Ability to tap into the data flow without changing the directed graph- Direct Address
Back pressue is used such that large data is processed incrementally - Back Pressure

## Comparison to Spreadsheet

An attempt at a universal language...lets see how it sits alongside excel

### Comparable to spreadsheet
- Joining - Arguably the language supports this better than excel
- Ease of Data Entry - continue to use excel where data entry is required
- Interoperablity : based on language which provides good interoperability to web services, file systems, data sources.
- Flexibility - base on general purpose programming language we can make it do whatever we want

### Needs further work
- Accessibility : It is accessible in all users web browsers and users data can be shared using sharing mechanism but getting shared data into the browser involves navigating the data access security mechanism which is going to be a problem.
- Self contained distribution : PowerBI provides a roadmap here...build functionality to take a model and create a new self contained model which can be distributed 
- Ease of presentation : Power Bi provides a roadmap here...maybe each stream should have a default chart presentation as part of the language....maybe which renders to console or browser depending on where the stream is running.
- Backwards compatibility : This is not a signficant issue currently. Solve when it becomes an issue.

### Avoiding excel issues
- Structured Logic - A significant advantage over excel is the ability to structure reusable logic avoiding repetition and copy and paste issues.
- Testability - Focus on end to end testing should always be foremost with the language
- Automation - Copy and paste as not even supported as a paradigm when you can simple split any stream. 

### Opportunities to leap ahead of spreadsheet
- Real Time Support - continuous data feed and large data sets not supported at all with spreadsheets
- Structure Logic Presentation : presenting the logic of the pipeline graphically....other declarative languages provide this (SQL for example) ...and it should be something that you get for free as a result of the development activities.

## Next Steps

Buid a CLI which makes it easy for users to
- get data and models from others
- run models (which may need to get data)
- share models/data with others
